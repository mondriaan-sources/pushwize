Pod::Spec.new do |s|
    s.name              = 'Pushwize'
    s.version           = '1.1.5'
    s.summary           = 'Pushwize iOS SDK'
    s.homepage          = 'https://mondriaan.com/'
    s.author            = { 'Levente Dimény' => 'levente.dimeny@mondriaan.com' }
    s.license           = 'Proprietary'
    s.platform          = :ios
    s.source            = { :http => "https://pods.sanomamdc.com/Pushwize/Pushwize-1.1.5.zip" }
    s.ios.deployment_target = '9.0'
    s.module_name = 'Pushwize'

    s.subspec 'Common' do |common|
    common.vendored_frameworks = 'Pushwize/PushwizeCommon.xcframework'
    common.frameworks = 'Security'
    end

    s.subspec 'Core' do |core|
    core.vendored_frameworks = 'Pushwize/Pushwize.xcframework'
    core.ios.resource_bundle = {'PushwizeResources' => 'Pushwize/PushwizeResources.bundle/*.nib'}
    core.frameworks = 'Foundation', 'UIKit', 'CoreLocation'
    core.dependency 'ZIPFoundation', '0.9.11'
    core.dependency 'Pushwize/Common'
    end

    s.subspec 'NotificationService' do |notification_service|
    notification_service.vendored_frameworks = 'Pushwize/PushwizeNotificationService.xcframework'
    notification_service.frameworks = 'UserNotifications'
    notification_service.dependency 'Pushwize/Common'
    end
end
